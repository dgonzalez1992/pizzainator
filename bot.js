// Discord.js package
const Discord = require('discord.js');
const client = new Discord.Client();

// Token
client.login('NTgzNDY2MjcxMjI4NTU5MzYx.XRWPYA.qrh-4xIgTwF7DsuSEy-178ZSGZ8');

// Pizza API package
var pizzapi = require('dominos');

// Prefix for bot
var prefix = '-'

client.on('ready', () => {
  console.log(`Logged in as ${client.user.tag}!`);
});

client.on('message', msg => {
  // Ping Command
  if (msg.content === `${prefix}ping`) {
    msg.reply('pong');
  }

  // Get the door, it's Dominos
  if (msg.content === `${prefix}pizza`) {

    msg.author.send('What is the address for this order?').then(() => {
      const filter = m => msg.author.id === m.author.id;

      msg.author.dmChannel.awaitMessages(filter, { time: 60000, maxMatches: 1, errors: ['time'] })
        .then(messages => {
          var cusAddress = messages.first().content
          // Grab Customer Address
          msg.author.send(`You've entered: ${messages.first().content}\n` +
            `Now what city is this in?`);


          msg.author.dmChannel.awaitMessages(filter, { time: 60000, maxMatches: 1, errors: ['time'] })
            .then(messages => {
              var cusCity = messages.first().content
              // Grab Customer City
              msg.author.send(`You've entered: ${messages.first().content}\n` +
                `Now what's the state?`);


              msg.author.dmChannel.awaitMessages(filter, { time: 60000, maxMatches: 1, errors: ['time'] })
                .then(messages => {
                  var cusState = messages.first().content
                  // Grab Customer State
                  msg.author.send(`You've entered: ${messages.first().content}\n` +
                    `Now what's the Zip?`);


                  msg.author.dmChannel.awaitMessages(filter, { time: 60000, maxMatches: 1, errors: ['time'] })
                    .then(messages => {
                      var cusZip = messages.first().content
                      // Grab Customer Zip

                      // Find closest store and display it to customer
                      pizzapi.Util.findNearbyStores(
                        cusAddress + `, ` + cusCity + `, ` + cusState + `, ` + cusZip,
                        'Delivery',
                        function (storeData) {
                          console.log(storeData);
                          console.log(storeData.result)
                          var userAddress = storeData.result.Address.Street
                          var userCity = storeData.result.Address.City
                          var userState = storeData.result.Address.Region
                          var userZip = storeData.result.Address.PostalCode
                          var storeID = storeData.result.Stores[0].StoreID
                          var storeMaxDistance = storeData.result.Stores[0].MaxDistance
                          var storeAddress = storeData.result.Stores[0].AddressDescription
                          var storePhone = storeData.result.Stores[0].Phone

                          msg.author.send(`Great! Here's the full address you provided:\n` +
                            userAddress + `\n` +
                            userCity + `, ` + userState + ` ` + userZip + `\n\n` +
                            `Here's the closest store to you:\n` +
                            `Store #` + storeID + `\n` +
                            storeAddress + `\n` +
                            storePhone + `\n` +
                            `Estimated Distance: ` + storeMaxDistance + ` miles\n\n` +
                            `Does this all look correct? [yes] or [no]`);
                        }
                      );


                    })
                    .catch(() => {
                      msg.author.send('You did not enter any input! If you want to try ordering again, please use the `pizza` command.');
                    });
                })
                .catch(() => {
                  msg.author.send('You did not enter any input! If you want to try ordering again, please use the `pizza` command.');
                });
            })
            .catch(() => {
              msg.author.send('You did not enter any input! If you want to try ordering again, please use the `pizza` command.');
            });
        })
        .catch(() => {
          msg.author.send('You did not enter any input! If you want to try ordering again, please use the `pizza` command.');
        });
    })

  }
});